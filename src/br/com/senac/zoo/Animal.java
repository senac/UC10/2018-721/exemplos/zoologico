package br.com.senac.zoo;


public interface Animal {

    void andar();

    void falar();

}
