package br.com.senac.polimorfismo;

public class Professor extends Pessoa {

    @Override
    public void falar() {
        System.out.println("Professor Falando...");
    }

}
