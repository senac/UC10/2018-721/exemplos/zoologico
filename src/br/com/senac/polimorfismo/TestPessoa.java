package br.com.senac.polimorfismo;

import java.util.ArrayList;
import java.util.List;

public class TestPessoa {

    public static void main(String[] args) {

        Pessoa a = new Professor();
        Pessoa b = new Aluno();
        Aluno c = new Silas();
        Pessoa p = new Pessoa();

        c.falar();

    }

    private static void mandaFalar(Pessoa y) {
        y.falar();
    }

}
